import unittest
import A01 as lib

__author__ = "Nick Reynolds"
__maintainer__ = "Nick Reynolds"
__email__ = "nick.reynolds@mq.edu.au"


class Test(unittest.TestCase):
    song1 = {'title': 'Hey Ya', 'artist': 'Outkast', 'duration': 120}
    song2 = {'artist': 'Hermitude', 'duration': 93, 'title': 'The Buzz'}

    def test_add(self):
        songs = []
        lib.add_song(songs, self.song1['title'], self.song1['artist'], self.song1['duration'])
        self.assertEqual(songs, [self.song1])
        lib.add_song(songs, self.song2['title'], self.song2['artist'], self.song2['duration'])
        self.assertEqual(songs, [self.song1, self.song2])

    def test_delete(self):
        songs = []
        lib.add_song(songs, self.song1['title'], self.song1['artist'], self.song1['duration'])
        lib.add_song(songs, self.song2['title'], self.song2['artist'], self.song2['duration'])
        lib.delete(songs, 'No Match', 'No Match')
        self.assertEqual(songs, [self.song1, self.song2], "Deleting a song that doesnt exist shouldn't change the list")
        lib.delete(songs, self.song2['title'], self.song2['artist'])
        self.assertEqual(songs, [self.song1], "Song two should have been removed")
        lib.add_song(songs, self.song2['title'], self.song2['artist'], self.song2['duration'])
        lib.delete(songs, self.song1['title'], self.song1['artist'])
        self.assertEqual(songs, [self.song2], "Song one should have been removed")

    def test_delete_loop_over(self):
        songs = []
        lib.add_song(songs, self.song1['title'], self.song1['artist'], self.song1['duration'])
        lib.add_song(songs, self.song2['title'], self.song2['artist'], self.song2['duration'])
        lib.add_song(songs, self.song2['title'], self.song2['artist'], self.song2['duration'])
        lib.add_song(songs, self.song2['title'], self.song2['artist'], self.song2['duration'])
        lib.delete(songs, self.song2['title'], self.song2['artist'])
        self.assertEqual(len(songs), 1, "If I have multiple of the same song, it should get removed too, check your loop (don't delete things from a list that you're looping over!)")

    def test_play(self):
        songs = []
        index = 0
        lib.add_song(songs, self.song1['title'], self.song1['artist'], self.song1['duration'])
        lib.add_song(songs, self.song2['title'], self.song2['artist'], self.song2['duration'])
        self.assertEqual(lib.play(index, songs), "Now playing Hey Ya by Outkast")
        songs = []
        self.assertEqual(lib.play(index, songs), None, "An empty playlist should return None")
        lib.add_song(songs, self.song1['title'], self.song1['artist'], self.song1['duration'])
        index = 5
        self.assertEqual(lib.play(index, songs), None, "Index out of range should return None")

    def test_skip(self):
        songs = []
        index = 0
        index = lib.skip(index, songs)

        self.assertEqual(0, index, "Skipping with no songs should leave current song at 0")
        lib.add_song(songs, self.song1['title'], self.song1['artist'], self.song1['duration'])
        index = lib.skip(index, songs)
        self.assertEqual(0, index, "Skipping with one song should leave current song at 0")
        index = lib.skip(index, songs)
        self.assertEqual(0, index, "Skipping with one song should leave current song at 0")
        lib.add_song(songs, self.song2['title'], self.song2['artist'], self.song2['duration'])
        index = lib.skip(index, songs)
        self.assertEqual(1, index, "Skipping from first to second song should work 0 to 1")
        index = lib.skip(index, songs)
        self.assertEqual(0, index, "Skipping last song should return you to the first")

    def test_duration(self):
        songs = []
        self.assertEqual(0, lib.total_time(songs), "An empty list should return 0 for total time")
        lib.add_song(songs, self.song1['title'], self.song1['artist'], self.song1['duration'])
        self.assertEqual(self.song1['duration'], lib.total_time(songs), "Duration for the first song is wrong")
        lib.add_song(songs, self.song2['title'], self.song2['artist'], self.song2['duration'])
        duration = self.song1['duration'] + self.song2['duration']
        self.assertEqual(duration, lib.total_time(songs), "Duration for 2 songs is wrong")

    def test_search(self):
        songs = []
        self.assertEqual([], lib.search(songs, "Hermitude"), "Empty list should have no results")
        lib.add_song(songs, self.song1['title'], self.song1['artist'], self.song1['duration'])
        self.assertEqual([], lib.search(songs, "Hermitude"), "There should be no result for this")
        self.assertEqual([self.song1], lib.search(songs, "Outkast"), "I should get a result for this, matches artist")
        self.assertEqual([self.song1], lib.search(songs, "Hey Ya"), "I should get a result for this, matches title")
        lib.add_song(songs, self.song2['title'], self.song2['artist'], self.song2['duration'])
        self.assertEqual([self.song1], lib.search(songs, "Outkast"), "I should get a result for this, matches artist")
        self.assertEqual([self.song1], lib.search(songs, "Hey Ya"), "I should get a result for this, matches title")
        self.assertEqual([self.song2], lib.search(songs, "Hermitude"), "I should get a result for this, matches artist")
        self.assertEqual([self.song2], lib.search(songs, "The Buzz"), "I should get a result for this, matches title")
        self.assertEqual([self.song2], lib.search(songs, "Buzz"), "Should get result, partially matches title")
        self.assertEqual([self.song2], lib.search(songs, "Herm"), "Should get result, partially matches artist")
        self.assertEqual([self.song2], lib.search(songs, "BUZZ"), "Should get result, should be case insensitive!")
        self.assertEqual([self.song2], lib.search(songs, "HERM"), "Should get result, should be case insensitive!")
        self.assertEqual([self.song1, self.song2], lib.search(songs, "t"), "Should get two results!")

if __name__ == '__main__':
    unittest.main(verbosity=2)
