__author__ = "### YOUR NAME HERE ###"
__maintainer__ = "Nick Reynolds"
__email__ = "nick.reynolds@mq.edu.au"


def add_song(playlist, title, artist, duration):
    """
    Add a song (dictionary) to the playlist (list)
    The song should be a dictionary with title, artist and duration as the keys.

    playlist is a list of songs.
    title and artist are strings
    duration is an integer
    """
    pass


def delete(playlist, title, artist):
    """
    Delete songs from the playlist that match the title AND artist parameters.
    """
    pass


def play(song_index, playlist):
    """
    Returns a string of the current song using the song_index.
    Should be in the format Now playing <title> by <artist>.
    If there are no songs in the playlist, or the index is out of range, return None
    """
    pass


def skip(song_index, playlist):
    """
    Skips to the next track by incrementing (and returning) the song_index
    If the playlist is empty it should return 0.
    If the index is out of range it should return 0.
    """
    pass


def total_time(playlist):
    """
    Gives the total run time for the playlist. i.e. the sum of the durations.
    Should return 0 if the list is empty
    """
    pass


def search(playlist, term):
    """
    Given 'term' return a list of all songs which contain term
    in their title OR artist. Case insensitive.
    Should return an empty list if there are no results.
    e.g. "JACK" should match "Jack in The Box".
    """
    pass


if __name__ == '__main__':
    # You can play with your code here!
    songs = []
    index = 0
    add_song(songs, 'Feel Good Inc', 'Gorillaz', 180)
    print(play(index, songs))
